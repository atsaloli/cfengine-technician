# Restarting Hub Reporting Components

_Very_ rarely, it happens that the UI shows stale data (reports don't get updated)
and scheduled reports are not generated -- even though everything else is fine
(host health is fine, report collectin works).

When this happens, let your hub administrator know; there is a known issue
with the hub reporting subsystem (involving the Redis cache between report
collection by `cf-hub` and the Postgres database where the data ultimately
ends up) -- and restarting the reporting subsystem (or the entire hub)
clears this up.