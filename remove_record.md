### Removing a host record

When you [wipe and reinstall](wipe_and_reinstall.md) or
[re-key](rekey.md), you should remove the original host record from the
hub, otherwise it'll hang around stale (it'll show up as a non-reporting
host).

**NOTE: Please be very certain the record actually *should* be removed before doing so.**
Do not remove records simply because the host hasn't reported in a long time!  If you
do, you make further investigation of that host impossible as it won't be in CFEngine
inventory to be investigated.  This applies even more when removing records in bulk.

You can remove a host record the UI or the API.

Reference: [Remove host from the hub](https://docs.cfengine.com/docs/3.10/reference-enterprise-api-ref-host.html#remove-host-from-the-hub)

Example of removing it through the API: 

```
curl -k -u admin https://hub.example.com/api/host/SHA=678daea5f1069f597b4c35c4014237418b51bf07eb0d512beac858e23fb6a6b4 -X DELETE
```

See also:  [Removing CFEngine](removing_cfengine.md)
