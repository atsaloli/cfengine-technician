# Wiping and reinstalling CFEngine

If you have a wierd issue you can't figure out,
you can try re-installing CFEngine (although
this is NOT recommended, as it's better to get to the bottom of things
and understand what went wrong and fix it properly).

The basic procedure is:

0. Note the host's identity
1. Remove CFEngine package
2. Remove `/var/cfengine` (as there'll be files left over even after the package is removed)
3. Install CFEngine package and bootstrap the host to the hub
4. Remove (from the hub) the record for the original identity (noted in first step above)

#### Example: RHEL systems

```
HUBIP=$(cat /var/cfengine/policy_server.dat) # note hub IP
/etc/init.d/cfengine3 stop         # shutdown CFEngine
ps -ef |grep cf-                   # make sure it's really down
yum remove -y cfengine-nova        # remove package
rm -rf /var/cfengine/              # wipe CFEngine's work directory

# re-install the agent RPM
wget --no-clobber http://s3.amazonaws.com/cfengine.packages/quick-install-cfengine-enterprise.sh  &&
sudo bash ./quick-install-cfengine-enterprise.sh agent

cf-agent -B $HUBIP                 # bootstrap the agent to the hub
ps -ef | grep cf-  |grep -v grep   # make sure you see three cf- processes
```

Note -- the above will install the latest stable version of CFEngine.
For actual use (in your production systems), you may need to use a specific
version -- check with your local CFE experts.

#### Example of a successful bootstrap

Example of a successful bootstrap.  Notice that all 3 daemons are running
(even if they weren't before, bootstrapping starts them).

```
imahost# cf-agent -B $HUBIP
R: This autonomous node assumes the role of voluntary client
R: Updated local policy from policy server
R: Started the scheduler
2015-07-08T14:38:12+0000   notice: Bootstrap to '10.10.10.10' completed successfully!
imahost# ps -ef |grep cf- |grep -v grep
root     11245     1  0 14:38 ?        00:00:00 /var/cfengine/bin/cf-execd
root     11299     1  0 14:38 ?        00:00:00 /var/cfengine/bin/cf-serverd
root     11303     1  1 14:38 ?        00:00:00 /var/cfengine/bin/cf-monitord
imahost#
```

#### Remove the original record

See [Removing the original record](remove_record.md)
