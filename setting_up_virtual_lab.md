Linux Academy offers a 7 day free trial -- https://linuxacademy.com/join/index/index/plan/winter2017-2/term/12/

Alternatively, for a live training, we can provide two VMs.

Here are some notes on completing your two-node CFEngine lab:

- bootstrap your hub VM to the private IP of your hub VM
- find the public hostname of your hub VM using:
```bash
host $(curl -s icanhazip.com)
```
- on your hub VM, add a line mapping the hub's public hostname to its private IP address, e.g.:
```text
172.31.45.129   ec2-54-235-32-238.compute-1.amazonaws.com
```

If you don't do the above, you may get complaints about a hostname mismatch when you access the CFEngine UI on your hub.

- Access the CFEngine UI from your laptop/workstation using the public hostname, e.g., `https://ec2-54-235-32-238.compute-1.amazonaws.com`
