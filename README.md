# "CFEngine Technician" Course

Aleksey Tsalolikhin
<aleksey@verticalsysadmin.com>

8 Feb 2017

## Course Description

In this course, you will learn:
- The basics of CFEngine architecture
- How to install, run and troubleshoot CFEngine

## Course Outline

- [Special note on systemd](#special-note-on-systemd)
- [Introduction](#introduction)
- [Lab setup](#lab-setup)
- [Orientation](#orientation)
- [Troubleshooting](#troubleshooting)
- [Procedures](#procedures)
- [Bibliography](#bibliography)

## Special note on systemd

For newer systems with systemd  (e.g. RHEL 7, CoreOS), you will
manage the CFEngine service with the standard systemd tool "systemctl".

Example commands:

```bash
systemctl enable cfengine3
systemctl disable cfengine3
systemctl start cfengine3
systemctl stop cfengine3
```

instead of using:

```bash
chkconfig cfengine3 on
chkconfig cfengine3 off
service cfengine3 start
service cfengien3 stop
```

## Introduction

1. Live introductory briefing and Q&A session (CFEngine basics)
1. What CFEngine hubs does your organization have? What are they for?
1. Read [CF-Primer][cf-primer] up to the slide "Promise Theory Book"
1. Read [CFEngine Bootstrapping Primer][bootstrapping-primer]

[cf-primer]: http://www.digitalelf.net/cf-primer
[bootstrapping-primer]: http://verticalsysadmin.com/blog/184/

## Lab setup

To learn CFEngine, set up a
lab environment with two VMs, one playing the role of "hub"
(CFEngine policy server) and the other, "host"
(a server managed by CFEngine). You can use this sandbox environment to 
safely learn CFEngine.

The VMs will need to be able to connect to each other on TCP port 5308.



Exercises:

1. Create two VMs (such as on Linux Academy or AWS).  (If you are using Linux Academy, see [Setting up your Linux Academy lab](setting_up_virtual_lab.md).)
1. If you are re-using an existing VM, if they had CFEngine, [removing it](https://gitlab.com/atsaloli/cfengine-technician/blob/master/removing_cfengine.md).
2. Pick which VM will be the hub, and which the host.
3. Does your hub VM have a fully qualified hostname that corresponds to
  its public IP address? The hub package won't install without a fully
  qualified hostname; and if the hostname doesn't map to the public IP
  address, you'll have other issues.
3. Install hub on your hub VM:

    ```
    wget http://s3.amazonaws.com/cfengine.packages/quick-install-cfengine-enterprise.sh  && 
    sudo bash ./quick-install-cfengine-enterprise.sh hub && 
    sudo /var/cfengine/bin/cf-agent --bootstrap $(hostname)
    ```
3. Edit the `acl` in `/var/cfengine/masterfiles/controls/def.cf` to change `/16` 
to `/0` to allow all hosts to connect to the hub (i.e., disable the ACL), so that
your host VM can connect to the CFEngine file distribution service to pick up
policy updates.  (Or, to be more secure, put the subnet that your hub and host are in.)
3. Login to your hub UI over HTTPS (`admin`/`admin`) and change the password.
4. Install the agent RPM and bootstrap it to your hub: 

    ```
    wget http://s3.amazonaws.com/cfengine.packages/quick-install-cfengine-enterprise.sh  &&
    sudo bash ./quick-install-cfengine-enterprise.sh agent &&
    sudo /var/cfengine/bin/cf-agent --bootstrap <IP address of policy server>

    ```
5. Notice your host in the Hosts list in the UI, in the Hosts tab (if your host is not there, it should appear within 5-10 minutes -- after the host runs the agent and the hub collects the reports).
6. Notice your host in the Inventory Report in the UI, in the Reports tab.
7. Add a couple of columns to the Inventory Report.
8. Export the Inventory Report as a CSV.
9. Run the following as root on the hub so that you can access its Postgres database from the command line with `psql`:

```
printf %s\\n "SET host.identifier = 'default.sys.fqhost'; SET rbac.filter = '"\!"(any)';" >  ~/.psqlrc
echo "alias psql='PAGER=less LESS=-FRXSe PGDATABASE=cfdb PGUSER=cfpostgres PSQL_EDITOR=vim psql'" >> ~/.bashrc
bash # load the new .bashrc
psql # log in to the database
```

Now you should be able to query the database using SQL, e.g.:

```sql
select * from hosts;
```

Example:

```shell_session
root@ubuntu:~# psql -q -c 'select * from hosts;'
                               hostkey                                |         hostname         |   ipaddress    |      lastreporttimestamp      |     firstreporttimestamp      
----------------------------------------------------------------------+--------------------------+----------------+-------------------------------+-------------------------------
 SHA=c7861354e11c1ee41de6be6307167efc3600e1b26592cc49f31a2e2cc9a7d5c5 | myhost.example.com       | 172.17.0.2     | 2018-02-20 20:30:26.493651+00 | 2018-02-20 20:30:26.493651+00
 SHA=33c2c9b3ba68fe4b0c2a6231ad1db0b14403c498196e470fdaaed3302b2edf77 | myhub.example.com        | 192.168.23.208 | 2018-02-21 01:55:25+00        | 2018-02-20 02:09:18.815617+00
(2 rows)

root@ubuntu:~# 
```


## Orientation

### Running the agent

CFEngine runs the agent at regular intervals to monitor and maintain the health of the host.

You can also run the agent from the command line (usually as root, so that it has access to configure the system).

Do the following on the host and on the hub. What differences do you see? What causes them?

1. Run `cf-agent` to run the agent.
2. Run `cf-agent -IC` to run the agent and have it inform you (with colorized output) of any changes it makes to the system.
3. Run `cf-agent -KIC` to force the agent to check everything, bypassing any time locks (in other words, re-check things it would normally skip because it has already checked them recently)
4. Run `cf-agent -IC -D DEBUG` to enable any debug print statements in the CFEngine policy.
5. Run `cf-agent -v` to enable verbose output.
6. Run `cf-agent -d` to enable debug output (in the binary).


### How CFEngine identifies hosts

Read [Displaying a host's CFEngine identity](print_key.md)

Exercises:

1. Look in `/var/cfengine/ppkeys` on the host and notice the host's public/private key-pair
2. Run `cf-key -p /var/cfengine/ppkeys/localhost.pub` and notice the host id (SHA checksum)
3. Look in `/var/cfengine/ppkeys` on the hub and notice the hub's key-pair and the host's public key
4. Run `select * from hosts` as a Custom Report in the Reports tab in the hub UI. Notice the `hostkey` column.
5. Run `psql` on the hub to enter the Postgres database, and then run `select * from hosts`.

The UI filters out some broken hosts; you can only see them from `psql`.

### The connections database

CFEngine has a database which stores incoming and outgoing network connections.
It is called the "lastseen" database. It stores the last connection in each
direction for each host in the "lastseen" database.

The database format is LMDB ([Lightning Memory-Mapped Database](https://en.wikipedia.org/wiki/Lightning_Memory-Mapped_Database)),
which is, roughly speaking, a high-performance key-value store for embedded systems.

Exercises:

1. Run `ls -lh /var/cfengine/state/cf_lastseen.lmdb` and `file /var/cfengine/state/cf_lastseen.lmdb` to examine the "lastseen" database file
2. Run `cf-key -s` to dump the contents of the "lastseen" database on the host
3. Run `cf-key -s` to dump the contents of the "lastseen" database on the hub

What is the difference between 2 and 3 above? Why?

### What's our hub?

The hub hostname or address is stored in `/var/cfengine/policy_server.dat`

Exercise: On a CFEngine host, run `cat /var/cfengine/policy_server.dat` to see the host's hub address.

## Troubleshooting  

Read and practice the following in your course lab environment:

- [Checking CFEngine health on a host](checking_CFEngine_health.md)
- [Handling non-reporting hosts](handling_non-reporting_hosts.md)
- [Troubleshooting report collection](troubleshooting_report_collection.md)
- [Restarting hub reporting components](restarting_hub_reporting_components.md)

## Procedures

### Re-bootstrapping CFEngine

If you want to move a host from one hub to another, the steps are as follows:

1. [Make a note of which hub the host is connected to now.](https://gitlab.com/atsaloli/cfengine-technician/blob/master/removing_cfengine.md#note-the-ip-address-of-our-cfengine-policy-server)
2. Bootstrap it to a new hub by running: `cf-agent -B <hub>`
3. [Note the host id](https://gitlab.com/atsaloli/cfengine-technician/blob/master/removing_cfengine.md#note-our-cfengine-id)
4. [Remove the host's record from the original hub using the DELETE command](https://gitlab.com/atsaloli/cfengine-technician/blob/master/removing_cfengine.md#delete-host-record-from-the-cfengine-hub-using-the-rest-api)

### Re-installing CFEngine agent

Read [Wiping and re-installing CFEngine](wipe_and_reinstall.md)

Exercise: Re-install CFEngine agent on a host.

### Re-keying a host

Read [Re-keying a host](rekey.md)

Exercise: Display the CFEngine identity of your host. Re-generate its identity, and notice the new identity.

### Disabling (and re-enabling) CFEngine

Read [Disabling (and re-enabling) CFEngine](disabling_cfengine.md)

Exercises:

1. Disable CFEngine on a host.
2. Wait until it shows in the "Hosts Not Reporting" list in the hub UI. How long did that take?
3. Re-enable CFEngine on a host. How long does it take for it to drop off the "Hosts Not Reporting" list? How could you speed that up?

### Removing CFEngine

Read [Removing CFEngine](removing_cfengine.md)

Exercise: Remove CFEngine from your host (including deleting the host record from the hub).

## Final exercise

Find and handle 5 non-reporting hosts from an actual hub (not from the hub in your learning lab).

## Bibliography

- Presentation (slides) [CF-Primer](http://digitalelf.net/cf-primer) by Brian Bennett and Aleksey Tsalolikhin
- Booklet [Modern Infrastructure Engineering with CFEngine 3](https://www.usenix.org/short-topics/modern-infrastructure-engineering-cfengine-3) by Mark Burgess and Diego Zamboni (85 pages)
- Book [Learning CFEngine 3](http://cf-learn.info/) by Diego Zamboni
- Tutorial (code examples and exercises) [CFEngine Tutorial](http://www.cfenginetutorial.org) by Aleksey Tsalolikhin
- Book [Thinking in Promises](http://shop.oreilly.com/product/0636920036289.do) by Mark Burgess
