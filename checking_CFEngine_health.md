# Checking CFEngine health on a host


## Summary

Run `cf-diag` to run a test suite containing various host health checks.

Run `cf-info` to get diagnostic information.

Most of the rest of this procedure was written before we had these two tools,
and is still useful in explaining what we check and why. But the quickest
way to diagnose host health issues is with the above two tools.

## Check CFEngine processes

You should see three cf- processes:

- `cf-execd`: executor (runs the agent at regular intervals)
- `cf-serverd`: server (makes reports available to the hub)
- `cf-monitord`: monitor (collects system information)

```
# ps -ef |grep cf- | grep -v grep
root     11243     1  0 Jul01 ?        00:16:47 /var/cfengine/bin/cf-execd
root     11268     1  0 Jul01 ?        00:22:31 /var/cfengine/bin/cf-serverd
root     11277     1  0 Jul01 ?        00:14:40 /var/cfengine/bin/cf-monitord
# 

```

Note: If you catch it just right, you may see `cf-execd` spawn a shell which runs
the update policy (update.cf) and then the regular policy.

Example:

cf-agent checking for policy updates:

```
root     13361     1  0 19:43 ?        00:00:00 /var/cfengine/bin/cf-execd
root     13386     1  0 19:44 ?        00:00:00 /var/cfengine/bin/cf-serverd
root     13395     1  0 19:44 ?        00:00:00 /var/cfengine/bin/cf-monitord
root     14830 13361  0 19:49 ?        00:00:00 sh -c "/var/cfengine/bin/cf-agent" -f "/var/cfengine/inputs/update.cf" ; "/var/cfengine/bin/cf-agent" -Dcf_execd_initiated -Dfrom_cfexecd
root     14831 14830 24 19:49 ?        00:00:00 /var/cfengine/bin/cf-agent -f /var/cfengine/inputs/update.cf
```

cf-agent running the policy:

```
root     13361     1  0 19:43 ?        00:00:00 /var/cfengine/bin/cf-execd
root     13386     1  0 19:44 ?        00:00:00 /var/cfengine/bin/cf-serverd
root     13395     1  0 19:44 ?        00:00:00 /var/cfengine/bin/cf-monitord
root     14830 13361  0 19:49 ?        00:00:00 sh -c "/var/cfengine/bin/cf-agent" -f "/var/cfengine/inputs/update.cf" ; "/var/cfengine/bin/cf-agent" -Dcf_execd_initiated -Dfrom_cfexecd
root     14842 14830  9 19:49 ?        00:00:00 /var/cfengine/bin/cf-agent -Dcf_execd_initiated -Dfrom_cfexecd
```

After running, cf-agent exits, leaving the original three:

```
root     13361     1  0 19:43 ?        00:00:00 /var/cfengine/bin/cf-execd
root     13386     1  0 19:44 ?        00:00:00 /var/cfengine/bin/cf-serverd
root     13395     1  0 19:44 ?        00:00:00 /var/cfengine/bin/cf-monitord 
```

You might also see `cf-promises`, the syntax checker CFEngine uses to
to validate policy.

This is all normal.

## Deviations from normal, and how to handle them

### No cf- processes


Example:

```shell_session
# ps -ef |grep cf- | grep -v grep
#
```

How to handle it: start CFEngine service.

```shell_session
# service cfengine3 start
Starting cf-execd:                                         [  OK  ]
Starting cf-serverd:                                       [  OK  ]
Starting cf-monitord:                                      [  OK  ]
#
```

### Many cf-agent processes, or an incomplete set of cf- daemons, or too many cf- daemons (i.e., more than one cf-execd or cf-monitord)


Example (missing cf-execd daemon):

```shell_session
# ps -ef |grep cf- | grep -v grep
root     11268     1  0 Jul01 ?        00:22:31 /var/cfengine/bin/cf-serverd
root     11277     1  0 Jul01 ?        00:14:40 /var/cfengine/bin/cf-monitord
#
```

How to handle it: Stop CFEngine, make sure all CFEngine processes exit,
and then start CFEngine.

```shell_session
# /etc/init.d/cfengine3 stop
Shutting down cf-execd:                                    [  OK  ]
Shutting down cf-serverd:                                  [  OK  ]
Shutting down cf-monitord:                                 [  OK  ]
# ps -ef |grep cf- |grep -v grep
# /etc/init.d/cfengine3 start
Starting cf-execd:                                         [  OK  ]
Starting cf-serverd:                                       [  OK  ]
Starting cf-monitord:                                      [  OK  ]
#
```

The next sections cover some other things you can check.

## Make sure CFEngine agent ran recently

Check the last time that the CFEngine agent ran by the timestamp
on its /var/cfengine/promise_summary.log. Compare that to the current system 
time. By default, CFEngine runs every 5 minutes.

Example:

```shell_session
# ls -lh /var/cfengine/promise_summary.log
-rw------- 1 root root 846K Jul  8 14:02 /var/cfengine/promise_summary.log
# date
Wed Jul  8 14:04:44 GMT 2015
#
```
If the runlog is out of date by more than 15 minutes and cf-execd
process is present, let your local CFEngine sysadmin know.

## Check agent version

Check  with "cf-agent -V":

```
# cf-agent -V
CFEngine Core 3.7.3
CFEngine Enterprise 3.7.3
#
```
Note: CFEngine software follows the model of free community core +
proprietary extensions. The version numbers should be the same,
or something is wrong.

The currently supported versions (LTS) are 3.7.x and 3.10.x.

If you see an older version (3.6 or earlier), or if the Core and Enterprise
versions don't match, [re-install CFEngine](wipe_and_reinstall.md) using
a current version.

## Check for full or read-only root filesystem

If the root filesystem is full or read-only, CFEngine won't work
properly (it won't be able to create temp files, prepare reports, 
write to its internal CSV databases (e.g. list of software installed) 
or log files).

Check for read-only root filesystem:

```
touch /tmp/cf_test; rm /tmp/cf_test
```

Check for full filesystem:

```
df -h /
```

## Make sure agent can download from the hub

Check that agent can receive updates by removing the
flag file that CFEngine will attempt to copy during
every update run.  You should see an info message
that the flag file you removed was updated (copied)
from the master on the policy hub:

```
# rm -f /var/cfengine/inputs/cf_promises_validated
# cf-agent -IC -f update.cf
    info: Copying from '10.10.10.10:/var/cfengine/masterfiles/cf_promises_validated'
#
```

Note: when CFEngine repairs the flag file, it will then check the rest
of the locally stored policy to be sure it matches what's on the hub.
If the local policy was "out of sync" with the hub (which is an error
condition), removing the flag file will fix it and you will see
additional files copied:

```
# rm -f /var/cfengine/inputs/cf_promises_validated
# cf-agent -IC -f update.cf
    info: Copying from '10.10.10.10:/var/cfengine/masterfiles/cf_promises_validated'
    info: Updated '/var/cfengine/inputs/promises.cf' from source '/var/cfengine/masterfiles/promises.cf' on '10.10.10.10'
#
```

## Check output from previous CFEngine run for errors

```
cat /var/cfengine/outputs/previous
```

## Run cf-agent and see if you get any errors

```
cf-agent
```
What you do when you see errors depends on what the error is. If in doubt,
escalate.

## Check for CSV database corruption

If cf-agent errors out with complaints about "can't parse CSV file", this is due
to CSV database corruption. CFEngine stores some reporting data in CSV files
until it delivers them to the hub. When the host disk gets full, these databases
sometimes get corrupted, and stay corrupted even after clearing disk space.

The solution is to shut down the CFEngine service, wipe /var/cfengine/state/
and restart the CFEngine service. CFEngine will regenerate the contents of
/var/cfengine/state.

```
service cfengine3 stop
rm -rf /var/cfengine/state/
service cfengine3 start
```

## Tooling: cf-info and cf-diag


Install `cf-diag` on your host:

```
dzdo wget --no-check-certificate -O /var/cfengine/bin/cf-diag \
  https://raw.githubusercontent.com/cfengine/core/master/contrib/cf-diag/cf-diag.sh
dzdo chmod a+rx 
```


There are two tools to aid operators in diagnosing host health issues
that may affect CFEngine operation: `cf-info` and `cf-diag`.

`cf-info` should already be present on most systems; `cf-diag` will
be added in the near future.

`cf-info` shows the output of different checks; `cf-diag` goes a 
step further and gives you an ok/fail for each check.

Example `cf-info` run:

```
[root@host /]# cf-info

CFEngine Core 3.10.3
CFEngine Enterprise 3.10.3

root       6173      1  0 21:16 ?        00:00:00 /var/cfengine/bin/cf-execd
root       6216      1  0 21:16 ?        00:00:00 /var/cfengine/bin/cf-monitord
root       6239      1  0 21:16 ?        00:00:00 /var/cfengine/bin/cf-serverd

Policy version:
2018.02.20
963f312ec54130ebcdcaa88207a8333a6ca4ede6

Bootstrapped to: hub.example.com

Policy channel assignment: Not assigned

Host key:
SHA=c7861354e11c1ee41de6be6307167efc3600e1b26592cc49f31a2e2cc9a7d5c5

Last cf-agent run started 27 minutes ago and lasted 2 seconds

update.cf compliance:   97% kept, 0% repaired, 3% not kept (out of 35 events)
promises.cf compliance: 90% kept, 3% repaired, 7% not kept (out of 196 events)

Filesystem writeable

Filesystem utilization: / 85%

[root@host /]# 
```


Example `cf-diag` run:

```
[root@host1 ~]# cf-diag
# SHA=a7732c134a682b5feae75d31087b1a9a9e802230f07f08f540460a541235e041
ok 1 - Expected Core version
ok 2 - Expected Enterprise version
ok 3 - Core version matches Enterprise version
# CFEngine Core 3.7.3
# CFEngine Enterprise 3.7.3
ok 4 - cf-execd running
ok 5 - cf-monitord running
ok 6 - cf-serverd running
ok 7 - cf-agent ran in last 15 minutes
ok 8 - Bootstrapped to hub (policy_server.dat exists)
# Hub IP is 1.2.3.4
ok 9 - Hub is connecting to host
ok 10 - Hub connected to host within last 15 minutes
# Has been 125 seconds since last connection
ok 11 - Filesystem has free space
# Disk usage is 28%
ok 12 - Filesystem is writable
ok 13 - promises.cf exists
ok 14 - update.cf exists
ok 15 - promises.cf is runnable
ok 16 - update.cf is runnable
ok 17 - Update run fixes changes to policy
1..17
[root@host1 ~]#
```


## RPM database corruption

If `rpm -qa` hangs or exits with errors, or if `yum` complains that /bin/bash
is missing, see [Troubleshooting RPM database corruption](troubleshooting_rpm_database_corruption.md)

