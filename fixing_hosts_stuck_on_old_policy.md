# Fixing hosts that are stuck on old policy (won't pick up new policy)

<!-- this is not linked to from the README.md as it is a little advanced -->

Due to a weakness in the CFEngine file transfer protocol,
hosts can end up stuck with a partially updated policy set.
They won't pick up a policy update even though it's available
on the hub.

This is noticeable when you look at Policy Release ID in Inventory
or if you've added your own human-readable policy name, e.g.,
"v.1.9.3", as an inventory item.

What happens is a host starts to update their `/var/cfengine/inputs`,
and copies `cf_promises_validated` from the hub (it’s at the top level
so it gets copied over early) and then for some reason (e.g., network blip)
the file transfer does not finish. On the next `cf-agent` update policy
run, the host won’t re-try updating its policy because
`cf_promises_validated` will match what’s on the hub (remember,
`cf_promises_validated` is how hosts decide whether they need to
scan the hub files for updates to copy over to the host).

A workaround is:

1. Deploy a new policy release
2. Wait half an hour or so
3. Remove and regenerate “cf_promises_validated” on the hub to force another update
4. Repeat steps 2 and 3 until all hosts show the new Policy Release ID.

Example of deleting and regenerating `cf_promises_validated`, on a hub:

```
[root@hub ~]# ls -lh /var/cfengine/masterfiles/cf_promises_validated
-rw------- 1 root root 29 May  8 19:29 /var/cfengine/masterfiles/cf_promises_validated
[root@hub ~]# cat /var/cfengine/masterfiles/cf_promises_validated
{
  "timestamp": 1525807747
[root@hub ~]# date -d @1525807747
Tue May  8 19:29:07 GMT 2018
[root@hub ~]# rm /var/cfengine/masterfiles/cf_promises_validated
rm: remove regular file `/var/cfengine/masterfiles/cf_promises_validated'? yes
[root@hub ~]# cf-agent -f update.cf -K
[root@hub ~]# ls -lh /var/cfengine/masterfiles/cf_promises_validated
-rw------- 1 root root 29 May  8 19:36 /var/cfengine/masterfiles/cf_promises_validated
[root@hub ~]#
```

See also: [CFEngine FAQ: Why are remote agents not updating?](https://docs.cfengine.com/docs/master/guide-faq-why-are-remote-agents-not-updating.html)

The vendor is considering alternative file transfer protocols: https://tracker.mender.io/browse/CFE-2058
