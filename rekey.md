# Re-keying a host


## Background

Host identities are supposed to be unique. If you end up with multiple hosts that have
the same identity (as happens when a VM that has CFEngine is cloned), this procedure
explains how to re-generate the CFEngine identity of a host.

Also, the Splunk forwarder has a GUID with the same problem from cloning:
when it's not done Splunk sees multiple hosts as the same client, just as
with CFEngine.  So regenerate the Splunk GUID as well.

## Procedure

1. Regenerate CFEngine key pair

    Run the following (as root) on the host whose identity you want to change:

    ```bash
    /sbin/service cfengine3 stop
    /var/cfengine/bin/cf-key -p /var/cfengine/ppkeys/localhost.pub # print the public key, you'll need it to delete the old record
    /bin/rm /var/cfengine/ppkeys/localhost.p*  # delete the key-pair
    /var/cfengine/bin/cf-key # generate new key
    /sbin/service cfengine3 start
    ```

2. Remove the record corresponding to the original key (which we deleted) per 
the procedure [Removing a host record](remove_record.md)

3. Re-generate Splunk forwarder GUID:

    ```bash
    /sbin/service splunkforwarder stop
    /opt/splunkforwarder/bin/splunk clone-prep-clear-config
    /sbin/service splunkforwarder start
    ```

    This will generate new GUIDs for the forwarders and they should then
    show up in Splunk.
