# Troubleshooting the host-to-hub link

## Background

This document is a collection of procedures that may be useful in troubleshooting policy update issues.

## Procedures

Run the following on the host (not the hub):

### Check if we are bootstrapped, and if so, to which hub:

```
imahost# strings /var/cfengine/policy_server.dat
10.10.10.10
imahost#
```

Is this the right hub?

### Dump the "lastseen" database (all connections in and out)

```
imahost# cf-key -s
Direction  IP                                       Name                      Last connection            Key
Incoming   10.10.10.10     imahub.example.com Tue Oct 17 23:38:24 2017   SHA=a40ebe4ad8cce926b6ab94e4c3687b01ebd0719f28dc38e6aaec8099a48c759a
Outgoing   10.10.10.10     imahub.example.com Tue Oct 17 23:33:18 2017   SHA=a40ebe4ad8cce926b6ab94e4c3687b01ebd0719f28dc38e6aaec8099a48c759a
Total Entries: 2
imahost#
```

Normally, you should just see two entries -- our outbound connection for policy updates,
and an incoming connection from the hub for report collection.

### Check when we last connected to our hub (checking for policy updates)

```
imahost# cf-key -s |grep ^Out
Outgoing   10.10.10.10     imahub.example.com Tue Oct 17 23:33:18 2017   SHA=a40ebe4ad8cce926b6ab94e4c3687b01ebd0719f28dc38e6aaec8099a48c759a
imahost#
```

We should be checking in for policy updates at regular intervals.  How long
ago did we last connect to our hub?

### Check when our hub last connected to us (for report collection)

```
imahost# cf-key -s |grep ^In
Incoming   10.10.10.10     imahub.example.com Tue Oct 17 23:38:24 2017   SHA=a40ebe4ad8cce926b6ab94e4c3687b01ebd0719f28dc38e6aaec8099a48c759a 
imahost#

```

The hub should be collecting reports from us periodically. How long ago did
the hub last connect to us?

### Check TCP connectivity to our hub

We need to be able to connect to the hub on TCP port 5308 (cfengine) to
download policy updates:

```
# telnet $( cat /var/cfengine/policy_server.dat ) cfengine
Trying 10.10.10.10...
Connected to 10.10.10.10.
Escape character is '^]'.

```

### Check we can get policy updates

Run the following command to force a policy update:

`cf-agent -KIC -D DEBUG,validated_updates_ready -f update.cf -vl`

You should see messages like this if our files are up to date:

```
File '/var/cfengine/inputs/inventory/macos.cf' is an up to date copy of source
```

Or like this if our copy is out of date:

```
Copying from '10.10.10.10:/var/cfengine/masterfiles/inventory/macos.cf'
...
Updated file from '10.10.10.10:/var/cfengine/masterfiles/inventory/macos.cf'
```