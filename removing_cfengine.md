# Removing CFEngine from a host

## Turn off CFEngine:

    /etc/init.d/cfengine3 stop

on newer systems (e.g., RHEL 7) if you're using CFEngine 3.10 or newer, instead use:

    systemctl stop cfengine3

## Make sure there are no CFEngine processes left:

    ps -ef | grep cf-

## Note the IP address of the CFEngine Policy Server

    HUB_IP=$(cat /var/cfengine/policy_server.dat)
    echo $HUB_IP
    
## Note the CFEngine id

    CFE_ID=$(sudo /var/cfengine/bin/cf-key -p /var/cfengine/ppkeys/localhost.pub)
    echo $CFE_ID

## Remove CFEngine package

    yum -y remove cfengine-nova

### See the hub's record for the host:

    /usr/bin/curl -k https://$HUB_IP/api/host/$CFE_ID --user <username>
    
## Delete host record from the CFEngine Hub using the REST API

Delete the record from the hub, so that the hub doesn't try to collect 
reports from the host:

    /usr/bin/curl -k https://$HUB_IP/api/host/$CFE_ID -X DELETE --user <username>

### Confirm the hub no longer has a record for this host:

    /usr/bin/curl -k https://$HUB_IP/api/host/$CFE_ID --user <username>

The hub output should say count = 0.  For example:

```bash
$ /usr/bin/curl -k https://$HUB_IP/api/host/$CFE_ID --user aleksey
Enter host password for user 'aleksey':
{
  "data": [],
  "meta": {
    "count": 0,
    "page": 1,
    "timestamp": 1467937898,
    "total": 0
  }
}$
```

If the hub still has a record for this host, it will show the record.
