# Handling RPM database corruption

## Background

Red Hat systems can end up with corrupted RPM databases.

When that happens, `rpm -qa` hangs or exits with errors. If `rpm` hangs,
`cf-agent` processes may stack up.

Another possible symptom is `yum` complains about missing dependencies
which are obviously there, e.g.:

```
Q: "...sr/bin/yum -y i": udev-147-2.40.el6.x86_64 has missing requires of /bin/bash
Q: "...sr/bin/yum -y i": usbutils-003-4.el6.x86_64 has missing requires of /bin/bash
Q: "...sr/bin/yum -y i": util-linux-ng-2.17.2-12.4.el6.x86_64 has missing requires of /bin/bash
```

## Procedure

Delete and re-create the RPM databases:

```
/bin/rm -rf /var/lib/rpm/__db.* && /bin/rpm --rebuilddb
```
It would be nice if that could be automated, but since `cf-agent` runs `rpm`
when it starts (during its system discovery phase), it hangs and never gets to 
the policy that would fix the issue.