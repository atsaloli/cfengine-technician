# Handling Hosts that are not Reporting

See Also:
- [Checking CFEngine Health on a Host](checking_CFEngine_health.md)
- [Troubleshooting Report Collection](troubleshooting_report_collection.md)

Intended audience: CFEngine Technicians

## Table of Contents
- [Background](#background)
  - [Hosts that NEVER reported](#hosts-that-never-reported)
  - [Missing ipaddress in hosts table](#missing-ipaddress-in-hosts-table)
  - [Hosts that HAD REPORTED in the past (but aren't reporting now)](#hosts-that-had-reported-in-the-past-but-aren-t-reporting-now)
- [Procedure](#procedure)
  - [Identify stale records](#identify-stale-records)
  - [Delete "empty" records](#delete-empty-records)
  - [Delete records of decommissioned hosts](#delete-records-of-decommissioned-hosts)
  - [Troubleshoot broken hosts](#troubleshoot-broken-hosts)
  - [Collect reports](#collect-reports)

## Background

Every time the hub collects a report from a host, it updates the
`lastreporttimestamp` field for that host in the `hosts` table.

Under ideal circumstances, every host record on each hub will have a recent
`lastreporttimestamp` (no more than double the poll
interval, which is the `hub_schedule` setting in `cf_hub.cf` under
`controls/` -- default is every 5 minutes.)


Here's how to identify and handle hosts
that have stopped reporting and now have a stale `lastreporttimestamp`.

### Hosts that NEVER reported

When a remote cf-agent first connects (during bootstrap), the hub
creates an entry for the host in the "hosts" table and
initializes the "firstreporttimestamp" and "lastreporttimestamp"
(same value).

Afterwards, every time the hub connects to the host to
download reports, it updates "lastreporttimestamp".

You'll notice some host records have the `lastreporttimestamp` same as
`firstreporttimestamp`. 

If the hub can't then download reports from the host, it won't
update `lastreporttimestamp` (this means it will be same as 
`firstreporttimestamp`), and there'll be no inventory detail
(including hostname!).

When "firstreporttimestamp" is the same as "lastreporttimestamp",
the hub does not have inventory for the host. For example:

```
cfdb=# select * from inventory where hostkey = 'SHA=a7fe874ab99cff4a8e1c8abd3aeda8b670e2ea472899f04f73d0f703d9694119';
 hostkey | keyname | type | metatags | value
---------+---------+------+----------+-------
(0 rows)

cfdb=#
```


### Missing `ipaddress` in `hosts` table

The IP address in the `hosts` table is _not_ from the inventory
gathered by cf-agent on the remote host -- it is the address
from which the remote cf-agent last checked in for a policy update.
This IP address is kept by the hub for up to seven days, and then deleted.

Missing `ipaddress` means the hub has not seen the host connect
(to the hub) for over a week.

If there is no `ipaddress` in the `hosts` table, and no inventory detail,
it means the hub can't connect to the host to download reports (because it does
not know the host's hostname or IP address).

### Hosts that HAD REPORTED in the past (but aren't reporting now)

If the host does not have an `ipaddress` but its
`lastreporttimestamp` is greater than `firstreporttimestamp`
this means the hub was able to collect from this host at least once and
you should be able to pull the IP address out of the host's inventory detail.

## Procedure

### Identify stale records

On each hub, run the following daily to identify stale records:

```sql
cfdb=# SELECT *
FROM hosts
WHERE
  lastreporttimestamp <  now() - '30 min'::interval
ORDER BY
  lastreporttimestamp ASC
;
```

### Delete "empty" records

We have an ["empty" records query](empty-records.sql)
that lists hosts for which no investigation is possible due to lack of any and all of the following:
- ipaddress (from last connection by remote cf-agent)
- hostname
- inventory detail (including IP address from inventory)

Delete these host records as no further investigation is possible.

### Delete records of decommissioned hosts

If you have a hostname/ipaddress, do not remove the record until you confirm
what happened to the host.

The host could be having issues, such as the kernel panicked or the host
froze during auto-migration between vCenter hosts. If so, get the host
back up and on the network so that CFEngine can poll it.

It could be that the host was decommissioned and you weren't told.
If so, remove the host from the hub and find out why you weren't informed
at the time of the decommission (it's supposed to be part of the decommissioning process
to remove the host from its CFEngine hub).

Be 100% sure that the host was decommissioned before you remove its record!
Otherwise you could be dropping existing (but broken) hosts from inventory.

### Troubleshoot broken hosts

If the host was not decommissioned, fix it; get it online.

### Collect reports

To collect reports from a host, run the `cf-hub` report collection
tool on the hub.

Use `-H` to specify the hostname, and `-q` to specify the query type.
The available query types are `rebase` (collect all reports and tell
the host to zero out its database where it stores deltas since last
collection) or `delta` to pick up the deltas only. Use `rebase` here
as the host may have accumulated such
a backlog of deltas that cf-hub will time out trying to copy them.

If you don't get an error, it means the report collection succeeded.

Example of a successful collection:

```shell_session
imahub# cf-hub -H 10.10.10.11 -q rebase
imahub#
```

Example of an unsuccessful collection:

```shell_session
[root@cfehubtlsmsdc01 ~]# cf-hub -H 10.10.10.11 -q rebase
   error: Abort transmission: got " Unspecified server refusal (see verbose server output)" from 10.10.10.11
[root@cfehubtlsmsdc01 ~]# 
```

If you have trouble collecting, see [Troubleshooting Report Collection](troubleshooting_report_collection.md)
