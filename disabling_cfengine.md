# Disabling (and re-enabling) CFEngine on a host

## Background

Sometimes we disable CFEngine on a host to troubleshoot something, or to 
exonerate CFEngine -- here's a common scenario: 

"CFEngine is breaking my app."

"OK, let's turn off CFEngine and check your app. Oh, look, your app is still 
broken.  It's not CFEngine. Let's turn CFEngine back on."

(Don't forget to turn CFEngine back on!)


## Disabling CFEngine

Notify your CFEngine team that you are going to disable CFEngine on a host and why.

Disable CFEngine service

       
```bash
chkconfig cfengine3 off
```

Edit root crontab to remove CFEngine watchdog cron job (if present):

```bash
crontab -e
```

The watchdog cron job would look like this:

```text
*/5 * * * * [ -x /etc/init.d/cfengine3 ] && if ! pgrep cf-execd > /dev/null ; then /etc/init.d/cfengine3 restart >/dev/null ; fi # keep cfengine3 running. To turn off: service cfengine3 stop; chmod -x /etc/init.d/cfengine3
```


Stop CFEngine service:

```bash
/etc/init.d/cfengine3 stop
```


## Re-enabling CFEngine


```bash
chkconfig cfengine3 on
/etc/init.d/cfengine3 start
cf-agent # check the system, collect data for reports for the hub
```
