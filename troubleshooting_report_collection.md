# Troubleshooting Report Collection

See Also:
- [Checking CFEngine health on a host](checking_CFEngine_health.md)
- [Handling Hosts that are not Reporting](handling_non-reporting_hosts.md)
- [Restarting Hub Reporting Components](restarting_hub_reporting_components.md)


NOTE: If you come across any report collection issues not covered here, please let us know!

## Table of Contents
- [Unspecified server refusal](#unspecified-server-refusal)
- [SSL_read: receive timeout](#ssl_read-receive-timeout)
- [Connection reset by peer](#connection-reset-by-peer)
- [Connection refused](#connection-refused)

Troubleshooting report collection should start with an attempt to collect
"delta" reports (data gathered by the host since the last report collection),
as this is what the Hub usually tries first.

If that fails, try a full report collection (following which the host zeroes out 
its store of "deltas" accumulated since the prior report collection attempt).

Sometimes just doing a full report collection the host clears things up.

Other times you might get an error, such as those listed below (complete with
handling for each).

## Unspecified server refusal

Example:

```
imahub# cf-hub -H 10.10.10.11 -q full
   error: Abort transmission: got " Unspecified server refusal (see verbose server output)" from 10.10.10.11
imahub# 
```

This usually means the host does not trust the hub. 

Often, the host will be bootstrapped to a different hub (check `/var/cfengine/policy_server.dat` on the host).

If the host was rebootstrapped to a different hub, delete the record on
the source hub.

If that's not the problem, then the error message where it says "see
verbose server output", means kill `cf-serverd` on the host and restart
it with `cf-serverd -Fv` and then try polling the host again and see the
verbose cf-serverd output to find out what the error is -- as part of
CFEngine's secure design, the host does not expose the error condition
details over the network.

## SSL_read: receive timeout

Today, cf-hub has a built-in 10 second timeout.

If you see an error message like the below, it means you've hit the timeout:

```shell_session
[root@imahub ~]# cf-hub -q full -H 10.10.10.11
   error: SSL_read: receive timeout
   error: Failed to collect data. (recv: Success)
[root@imahub ~]#
```
Ignore the "recv: Success" part of the error, that's confusing and has been fixed in more recent versions of cf-hub.

We have asked the vendor to make it possible to increase the timeout.

## Connection refused

If you get connection refused, it could be a firewall issue or that `cf-serverd` is not running on that host.

If you run into anythine else, please escalate.