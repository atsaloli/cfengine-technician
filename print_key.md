# Show host identity

CFEngine generates a cryptographic key pair during installation.
These keys are used to secure communications between hub and host.

Also, CFEngine identifies hosts using a [SHA](https://en.wikipedia.org/wiki/Secure_Hash_Algorithms) checksum of the public key.
The keys are stored in `/var/cfengine/ppkeys`.

Run `cf-key -p /var/cfengine/ppkeys/localhost.pub` to print the host's identity.

For example:

```
# cf-key -p /var/cfengine/ppkeys/localhost.pub 
SHA=697daea5fx069f597b4c35c4014220418b51bf06eb0d512aeac856e2afb6a4b4
#
```
